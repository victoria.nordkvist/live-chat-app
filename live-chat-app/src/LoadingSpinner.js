import React from 'react';
import { Loader, Dimmer } from 'semantic-ui-react';

const LoadingSpinner = () => (
    <Dimmer active> 
    <Loader size="medium" content={"Loading ..."} />
    </Dimmer>
)

export default LoadingSpinner;