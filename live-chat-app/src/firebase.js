import firebase from 'firebase/app';
import "firebase/auth";
import "firebase/database";
import "firebase/storage";

  var config = {
    apiKey: "AIzaSyAJn8OBB4NksDIni8B9AC1TEmhVDMpU7uk",
    authDomain: "live-chat-app-e9403.firebaseapp.com",
    databaseURL: "https://live-chat-app-e9403.firebaseio.com",
    projectId: "live-chat-app-e9403",
    storageBucket: "live-chat-app-e9403.appspot.com",
    messagingSenderId: "937273803030"
  };
  firebase.initializeApp(config);

  export default firebase;
